/**
 * Student Elements program JS
 *
 * @package Billing Elements module
 */

// Fill Title and Amount fields from select input
var billingElementSelectFillTitleAmount = function( element_id ) {
	var $title = $( '#valuesnewTITLE' ),
		$amount = $( '#valuesnewAMOUNT' );

	if ( element_id <= 0 ) {
		$title.val( '' );
		$amount.val( '' );

		return;
	}

	var billingElements = JSON.parse($('#billing_elements').val());

	var element = billingElements[ element_id ];

	$title.val( element['TITLE'] );
	$amount.val( element['AMOUNT'] );
};

$('.onchange-billing-element-select').on('change', function() {
	billingElementSelectFillTitleAmount(this.value);
});
