/**
 * Mass Assign Elements program JS
 *
 * @package Billing Elements module
 */

// Fill Title and Amount fields from select input
var billingElementSelectFillTitleAmount = function( element_id, this_name ) {
	var input_number = this_name.replace('elements_id[', '').replace(']', '');

	var $title = $( '#title' + input_number ),
		$amount = $( '#amount' + input_number );

	if ( element_id <= 0 ) {
		$( 'select[name="' + this_name + '"]' ).val( '' );
		$title.val( '' );
		$amount.val( '' );

		return;
	}

	var billingElements = JSON.parse($('#billing_elements').val());

	var element = billingElements[ element_id ];

	$title.val( element['TITLE'] );
	$amount.val( element['AMOUNT'] );
};

// Add new Element inputs
var billingElementAddNew = function() {

	// Check to see if the counter has been initialized
	if (typeof billingElementAddNew.i == 'undefined') {
		// It has not... perform the initialization
		billingElementAddNew.i = 0;
	}

	billingElementAddNew.i++;

	var i = billingElementAddNew.i;

	var elementInputsClone = $( '<div>' ).append( $('#element_inputs' + i).clone() ).html(),
		j = i + 1;

	var elementInputsNew = elementInputsClone.replace( new RegExp( "\\[" + i + "\\]", 'g' ), '[' + j + ']' )
		.replace( new RegExp( 'element_inputs' + i, 'g' ), 'element_inputs' + j )
		.replace( new RegExp( 'elements_id' + i, 'g' ), 'elements_id' + j )
		.replace( new RegExp( 'title' + i, 'g' ), 'title' + j )
		.replace( new RegExp( 'amount' + i, 'g' ), 'amount' + j )
		.replace( new RegExp( 'comments' + i, 'g' ), 'comments' + j )
		.replace( new RegExp( 'Select' + i, 'g' ), 'Select' + j ) // Due date inputs.
		.replace( new RegExp( 'trigger' + i, 'g' ), 'trigger' + j ); // Due date icon.

	$( '<hr />' + elementInputsNew ).insertAfter('#element_inputs' + i);

	billingElementSelectFillTitleAmount( 0, 'elements_id[' + j + ']' );

	// Setup new calendar JS on icon click.
	JSCalendarSetup();
};

$('.onclick-billing-element-new-button').on('click', billingElementAddNew);

// Fix function called as many times as we browsed the page in AJAX / loaded this JS file
$(document).off('change', '.onchange-billing-element-select');
$(document).on('change', '.onchange-billing-element-select', function() {
	billingElementSelectFillTitleAmount(this.value, this.name);
});

// Get Element ID from URL.
// @link https://stackoverflow.com/questions/827368/using-the-get-parameter-of-a-url-in-javascript
var elementIdMatches = /element_id=([^&#=]*)/.exec(window.location.search);

billingElementSelectFillTitleAmount( ( elementIdMatches ? elementIdMatches[1] : 0 ), 'elements_id[1]' );
