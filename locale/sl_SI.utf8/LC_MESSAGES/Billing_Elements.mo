��            )   �      �     �     �  (   �     �     �     �  
             (     0     A     Q     Z     j          �     �     �     �  	   �     �     �  ,   �  (     G   7  -     L   �  F   �  %   A  �  g     R  	   _  /   i     �     �     �  
   �  
   �     �     �               $     5     S     a     q     }     �  	   �     �     �  &   �  %   �  4   	  .   =	  ;   l	  >   �	  #   �	                   
                                                                    	                                                  %d assigned %d purchases Add Element and Fee to Selected Students Assign Billing Elements Breakdown by Grade Level Categories Category Element Element Category Element and Fee Elements Leave a comment Mass Assign Elements My Elements New Category New Element Number of Elements Purchase Reference Store Student Elements There are no available seats in this course. You are already enrolled in this course. You are not enrolled in the right Grade Level to purchase this element. You child is already enrolled in this course. You child is not enrolled in the right Grade Level to purchase this element. You do not have sufficient funds to purchase this element. Balance: %s You have been enrolled in the course. Project-Id-Version: Billing Elements Module for RosarioSIS.
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 16:56+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SearchPath-0: .
 %d dodeljeno %d nakupi Dodajte element in pristojbino izbranim dijakom Dodeljen Elementi obračunavanja Razčlenitev po letniku Kategorija Kategorija Element Kategorija elementa Element in pristojbina Elementi Pustite komentar Elementi množične dodelitve Moji elementi Nova Kategorija Nov element Število elementov Nakup Referenca Trgovina Dijaški elementi V tej vzgojni skupini ni prostih mest. V to vzgojno skupino ste že vpisani. Niste vpisani v pravi letnik za nakup tega elementa. Vaš otrok je že vpisan v to vzgojno skupino. Vaš otrok ni vpisan v pravi letnik za nakup tega elementa. Nimate dovolj sredstev za nakup tega elementa.  Ravnovesje: %s Vpisani ste bili v vzgojno skupino. 