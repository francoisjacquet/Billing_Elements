��    !      $  /   ,      �     �     �  (        +     2     C  
   \     g  %   p     �     �     �     �     �     �     �     �               %  	   .     8     >  ,   O  +   |     �  (   �  G   �  -   -  L   [  F   �  %   �  �       �  
   �  1         2     ;     T  
   p  	   {  )   �     �     �     �  	   �     �     �     	     (	     7	     E	     Z	     b	     n	     s	  (   �	  &   �	     �	  (   �	  L   
  ,   _
  J   �
  C   �
                                                             !              
                                                                	                         %d assigned %d purchases Add Element and Fee to Selected Students Assign Billing Elements Breakdown by Grade Level Categories Category Do you want to purchase that element? Element Element Category Element and Fee Elements Leave a comment Mass Assign Elements My Elements New Category New Element Number of Elements Purchase Reference Store Student Elements There are no available seats in this course. This element was purchased. New balance: %s Total from Elements You are already enrolled in this course. You are not enrolled in the right Grade Level to purchase this element. You child is already enrolled in this course. You child is not enrolled in the right Grade Level to purchase this element. You do not have sufficient funds to purchase this element. Balance: %s You have been enrolled in the course. Project-Id-Version: Billing Elements Module for RosarioSIS.
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 16:55+0200
Last-Translator: Emerson Barros
Language-Team: RosarioSIS <info@rosariosis.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SearchPath-0: .
 %d atribuído %d compras Adicionar elemento e taxa aos alunos selecionados Atribuir Elementos de Faturamento Divisão por nível escolar Categorias Categoria Você quer comprar este elemento (item) ? Elemento Categoria do elemento Elemento e Taxa Elementos Deixe um comentário Atribuir elementos em massa Meus elementos Nova categoria Novo elemento Número de elementos Comprar Referência Loja Elementos do aluno Não há vagas disponíveis nesta turma. Este item foi comprado. Novo saldo: %s Total de elementos Você já está matriculado nesta turma. Você não está matriculado na série correta para poder comprar este item. Seu filho já está matriculado nesta turma. Seu filho não está matriculado na série correta para comprar este item. Você não tem fundos suficientes para comprar este item. Saldo: %s Você foi matriculado na turma. 