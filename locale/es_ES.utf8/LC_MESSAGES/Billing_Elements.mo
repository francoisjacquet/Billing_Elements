��    (      \  5   �      p     q     }  6   �  (   �     �     �          )     :  
   S     ^  %   g     �     �     �     �     �     �     �     �     �     	  ;        X     a  	   t     ~     �  ?   �  ,   �  +        .  (   B  G   k  -   �  L   �  F   .  %   u  %   �  �  �     �	  
   �	  D   �	  8   

     C
     K
  #   k
     �
     �
     �
  
   �
     �
     �
     �
       	   #     -     A     \     j     {     �  O   �     �     �  
               L   2  )     *   �     �      �  B     "   O  J   r  A   �      �  &                           %                     (                    '                    	   &                    
          "         $                          #                 !       %d assigned %d purchases %d students who have not been assigned the element yet Add Element and Fee to Selected Students Assign Assign Elements by Grade Level Assign Selected Elements Billing Elements Breakdown by Grade Level Categories Category Do you want to purchase that element? Element Element Category Element and Fee Elements Leave a comment Mass Assign Elements My Elements New Category New Element Number of Elements Only to students who have not been assigned the element yet Purchase Reconcile Payments Reference Store Student Elements The elements and fees have been added to the selected students. There are no available seats in this course. This element was purchased. New balance: %s Total from Elements You are already enrolled in this course. You are not enrolled in the right Grade Level to purchase this element. You child is already enrolled in this course. You child is not enrolled in the right Grade Level to purchase this element. You do not have sufficient funds to purchase this element. Balance: %s You have been enrolled in the course. You must choose at least one element. Project-Id-Version: Billing Elements Module for RosarioSIS.
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-07-22 17:52+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SearchPath-0: .
 %d asignados %d compras %d estudiantes para quienes el elemento no ha sido asignado todavía Agregar Elemento y Cobro a los Estudiantes Seleccionados Asignar Asignar los elementos por grado Asignar los elementos seleccionados Elementos de Facturación Análisis por Grado Categorías Categoría Quiere comprar este elemento? Elemento Categoría de Elemento Elemento y Cobro Elementos Dejar un comentario Asignar Elementos a Varios Mis Elementos Nueva Categoría Nuevo Elemento Número de Elementos Solamente los estudiantes para quienes el elemento no ha sido asignado todavía Comprar Conciliar los pagos Referencia Tienda Elementos Estudiante Estos elementos y cobros han sido asignados a los estudiantes seleccionados. No hay cupos disponibles para este curso. Ha comprado este elemento. Nuevo saldo: %s Total de los Elementos Usted ya esta inscrito al curso. No está inscrito en el Grado adecuado para comprar este elemento. Su hijo ya esta inscrito al curso. Su hijo no está inscrito en el Grado adecuado para comprar este elemento. No tiene suficientes fondos para comprar este elemento. Saldo: %s Usted ha sido inscrito al curso. Debe escoger por lo menos un elemento. 