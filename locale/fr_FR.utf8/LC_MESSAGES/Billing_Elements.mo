��    (      \  5   �      p     q     }  6   �  (   �     �     �          )     :  
   S     ^  %   g     �     �     �     �     �     �     �     �     �     	  ;        X     a  	   t     ~     �  ?   �  ,   �  +        .  (   B  G   k  -   �  L   �  F   .  %   u  %   �  �  �     �	  	   �	  >   �	  N   
  	   R
  +   \
  &   �
     �
      �
     �
  
   �
  "     	   $     .      E  
   f     q      �     �     �     �     �  N   �     @     H     a     m     u  Z   �  1   �  0        K  &   `  S   �  ,   �  Y     L   b  $   �  )   �                       %                     (                    '                    	   &                    
          "         $                          #                 !       %d assigned %d purchases %d students who have not been assigned the element yet Add Element and Fee to Selected Students Assign Assign Elements by Grade Level Assign Selected Elements Billing Elements Breakdown by Grade Level Categories Category Do you want to purchase that element? Element Element Category Element and Fee Elements Leave a comment Mass Assign Elements My Elements New Category New Element Number of Elements Only to students who have not been assigned the element yet Purchase Reconcile Payments Reference Store Student Elements The elements and fees have been added to the selected students. There are no available seats in this course. This element was purchased. New balance: %s Total from Elements You are already enrolled in this course. You are not enrolled in the right Grade Level to purchase this element. You child is already enrolled in this course. You child is not enrolled in the right Grade Level to purchase this element. You do not have sufficient funds to purchase this element. Balance: %s You have been enrolled in the course. You must choose at least one element. Project-Id-Version: Billing Elements Module for RosarioSIS.
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-07-22 17:52+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SearchPath-0: .
 %d attribués %d achats %d élèves pour qui l'élément n'a pas encore été assigné Ajouter l'élément et le frais de scolarité pour les élèves sélectionnés Attribuer Assigner les éléments par niveau scolaire Assigner les éléments sélectionnés Éléments de facturation Répartition par niveau scolaire Catégories Catégorie Voulez-vous acheter cet élément? Élément Catégorie d'élément Élément et frais de scolarité Éléments Laisser un commentaire Assigner des éléments en masse Mes éléments Nouvelle catégorie Nouvel élément Nombre d'éléments Seulement pour les élèves pour qui l'élément n'a pas encore été assigné Acheter Rapprocher les paiements Référence Magasin Éléments de l'élève Ces éléments et ces frais de scolarité ont été assignés aux élèves sélectionnés. Il n'y a pas de places disponibles pour ce cours. Cet élément a été acheté. Nouveau solde: %s Total des éléments Vous êtes déjà inscrit à ce cours. Vous n'êtes pas inscrit au bon niveau scolaire pour pouvoir acheter cet élément. Votre enfant est déjà inscrit à ce cours. Votre enfant n'est pas inscrit au bon niveau scolaire pour pouvoir acheter cet élément. Vous n'avez pas assez de fonds pour pouvoir acheter cet élément. Solde: %s Vous avez été inscrit à ce cours. Vous devez choisir au moins un élément. 