<?php
/**
 * Daily Transactions
 *
 * @since 12.0
 *
 * Merge Daily Transactions & Daily Totals programs
 *
 * @package RosarioSIS
 * @subpackage modules
 */

DrawHeader( ProgramTitle() );

$_REQUEST['program'] = issetVal( $_REQUEST['program'], '' );

if ( $_REQUEST['program'] === 'totals'
	&& User( 'PROFILE' ) === 'admin' )
{
	require_once 'modules/Billing_Elements/includes/DailyTotals.php';
}
else
{
	require_once 'modules/Billing_Elements/includes/DailyTransactions.php';
}


/**
 * Program Menu
 *
 * Local function
 *
 * @param  string $program Program: transactions|totals.
 *
 * @return string           Select Program input.
 */
function _programMenu( $program )
{
	global $_ROSARIO;

	if ( ! AllowEdit() )
	{
		$_ROSARIO['allow_edit'] = true;

		$allow_edit_tmp = true;
	}

	$link = PreparePHP_SELF(
		[],
		[ 'program' ]
	) . '&program=';

	$menu = SelectInput(
		$program,
		'program',
		'',
		[
			'transactions' => _( 'Daily Transactions' ),
			'totals' => _( 'Daily Totals' ),
		],
		false,
		'autocomplete="off" ' .
		( version_compare( ROSARIO_VERSION, '12.5', '>=' ) ?
			// @since RosarioSIS 12.5 CSP remove unsafe-inline Javascript
			'class="onchange-ajax-link" data-link="' . $link . 'this.value"' :
			'onchange="' . AttrEscape( 'ajaxLink(' . json_encode( $link ) . ' + this.value);' ) . '"' ),
		false
	);

	if ( ! empty( $allow_edit_tmp ) )
	{
		$_ROSARIO['allow_edit'] = false;
	}

	return $menu;
}
