<?php
/**
 * Daily Totals program
 *
 * @package RosarioSIS
 * @subpackage modules
 */

// Set start date.
$start_date = RequestedDate( 'start', date( 'Y-m' ) . '-01' );

// Set end date.
$end_date = RequestedDate( 'end', DBDate() );

// Set category ID.
$category_id = issetVal( $_REQUEST['category_id'], '' );

DrawHeader( _programMenu( 'totals' ) );

echo '<form action="' . URLEscape( 'Modules.php?modname=' . $_REQUEST['modname'] . '&program=totals'  ) . '" method="GET">';

DrawHeader( _( 'Report Timeframe' ) . ': ' .
	PrepareDate( $start_date, '_start', false ) . ' ' . _( 'to' ) . ' ' .
	PrepareDate( $end_date, '_end', false ) . ' ' . Buttons( _( 'Go' ) ) );

$categories_RET = DBGet( "SELECT ID,TITLE
	FROM billing_categories
	WHERE SCHOOL_ID='" . UserSchool() . "'
	ORDER BY SORT_ORDER IS NULL,SORT_ORDER,TITLE" );

$category_options = [];

foreach ( (array) $categories_RET as $category )
{
	$category_options[ $category['ID'] ] = $category['TITLE'];
}

$category_select = SelectInput(
	$category_id,
	'category_id',
	'',
	$category_options,
	_( 'All' ),
	'autocomplete="off" ' .
	( version_compare( ROSARIO_VERSION, '12.5', '>=' ) ?
		// @since RosarioSIS 12.5 CSP remove unsafe-inline Javascript
		'class="onchange-ajax-post-form"' :
		'onchange="ajaxPostForm(this.form, true);"' ),
	false
);

DrawHeader( '<label>' . _( 'Category' ) . ': ' . $category_select . '</label>' );

echo '</form>';

$fees_extra_where = " AND f.STUDENT_ID=bse.STUDENT_ID
	AND f.ID=bse.FEE_ID
	AND bse.ELEMENT_ID=be.ID
	AND be.SCHOOL_ID='" . UserSchool() . "'
	AND be.SYEAR='" . UserSyear() . "'";

if ( ! empty( $category_id ) )
{
	$fees_extra_where .= " AND be.CATEGORY_ID='" . (int) $category_id . "'";
}

$billing_fees = DBGetOne( "SELECT sum(f.AMOUNT) AS AMOUNT
	FROM billing_fees f,billing_student_elements bse,billing_elements be
	WHERE  f.SYEAR='" . UserSyear() . "'
	AND f.SCHOOL_ID='" . UserSchool() . "'
	AND f.ASSIGNED_DATE BETWEEN '" . $start_date . "'
	AND '" . $end_date . "'" . $fees_extra_where );

$billing_payments = 0;

if ( $billing_fees )
{
	// Include Students active as of Timeframe end date.
	$extra['DATE'] = $end_date;

	$fees_extra = $extra;

	$fees_extra['SELECT_ONLY'] = "f.ID";

	$fees_extra['FROM'] = issetVal( $fees_extra['FROM'], '' );
	$fees_extra['FROM'] .= ',billing_fees f,billing_student_elements bse,billing_elements be';

	$fees_extra['WHERE'] = issetVal( $fees_extra['WHERE'], '' );
	$fees_extra['WHERE'] .= " AND f.STUDENT_ID=s.STUDENT_ID AND f.SYEAR=ssm.SYEAR
		AND f.SCHOOL_ID=ssm.SCHOOL_ID AND f.ASSIGNED_DATE BETWEEN '" . $start_date . "' AND '" . $end_date . "'";

	$fees_extra['WHERE'] .= " AND f.STUDENT_ID=bse.STUDENT_ID
		AND f.ID=bse.FEE_ID
		AND bse.ELEMENT_ID=be.ID
		AND be.SCHOOL_ID='" . UserSchool() . "'
		AND be.SYEAR='" . UserSyear() . "'";

	if ( ! empty( $category_id ) )
	{
		$fees_extra['WHERE'] .= " AND be.CATEGORY_ID='" . (int) $category_id . "'";
	}

	$RET = GetStuList( $fees_extra );

	$payment_comments_where = '';

	if ( $RET )
	{
		// Reconcile Payments.
		// 1. Get fee IDs
		$fee_ids = [];

		foreach ( (array) $RET as $fee )
		{
			$fee_ids[] = $fee['ID'];
		}

		// 2. Get fee titles
		$fee_titles = DBGet( "SELECT DISTINCT TITLE
			FROM billing_fees
			WHERE SYEAR='" . UserSyear() . "'
			AND SCHOOL_ID='" . UserSchool() . "'
			AND ID IN(" . implode( ',', array_map( 'intval', $fee_ids ) ) . ")" );

		$payment_comments_where = [];

		foreach ( (array) $fee_titles as $fee_title )
		{
			$fee_title_escaped = DBEscapeString( $fee_title['TITLE'] );

			// 3. Where payment comments like fee title
			$payment_comments_where[] = "p.COMMENTS='" . $fee_title_escaped . "'
				OR p.COMMENTS LIKE CONCAT('%','" . $fee_title_escaped . "')
				OR p.COMMENTS LIKE CONCAT('" . $fee_title_escaped . "','%')";
		}

		$payment_comments_where = " AND (" . implode( " OR ", $payment_comments_where ) . ")";
	}

	$billing_payments = DBGetOne( "SELECT sum(p.AMOUNT) AS AMOUNT
		FROM billing_payments p
		WHERE p.SYEAR='" . UserSyear() . "'
		AND p.SCHOOL_ID='" . UserSchool() . "'
		AND p.PAYMENT_DATE BETWEEN '" . $start_date . "'
		AND '" . $end_date . "'
		" . $payment_comments_where );
}


echo '<br />';

PopTable( 'header', _( 'Totals' ) );

echo '<table class="cellspacing-5 align-right">';

echo '<tr><td>' . _( 'Payments' ) . ': ' .
	'</td><td>' . Currency( $billing_payments ) . '</td></tr>';

echo '<tr><td>' . _( 'Less' ) . ': ' . _( 'Fees' ) . ': ' .
	'</td><td>' . Currency( $billing_fees ) . '</td></tr>';

echo '<tr><td><b>' . _( 'Total' ) . ': ' . '</b></td>' .
	'<td><b>' . Currency( ( $billing_payments - $billing_fees ) ) . '</b></td></tr>';

echo '</table>';

PopTable( 'footer' );
