<?php
/**
 * Billing Elements common functions
 *
 * @package Billing Elements module
 */

/**
 * Get Element details from DB
 *
 * @param int     $element_id Element ID.
 * @param boolean $reset      Reset flag to reset cache.
 *
 * @return array Element details.
 */
function BillingGetElement( $element_id, $reset = false )
{
	static $elements = [];

	if ( (string) (int) $element_id != $element_id
		|| $element_id < 1 )
	{
		return [];
	}

	if ( isset( $elements[ $element_id ] )
		&& ! $reset )
	{
		return $elements[ $element_id ];
	}

	$element_RET = DBGet( "SELECT ID,CATEGORY_ID,TITLE,REF,AMOUNT,
		DESCRIPTION,GRADE_LEVELS,COURSE_PERIOD_ID,ROLLOVER,CREATED_AT,UPDATED_AT,
		CONCAT(coalesce(NULLIF(CONCAT(REF,' - '),' - '),''),TITLE) AS REF_TITLE,
		(SELECT TITLE
			FROM billing_categories
			WHERE ID=CATEGORY_ID) AS CATEGORY_TITLE
		FROM billing_elements
		WHERE ID='" . (int) $element_id . "'
		AND SCHOOL_ID='" . UserSchool() . "'
		AND SYEAR='" . UserSyear() . "'" );

	$elements[ $element_id ] = ( ! $element_RET ? [] : $element_RET[1] );

	return $elements[ $element_id ];
}

/**
 * Get all Elements from DB for current School and Year.
 *
 * @return array Elements.
 */
function BillingGetElements()
{
	$elements_RET = DBGet( "SELECT be.ID,be.TITLE,be.REF,be.AMOUNT,be.DESCRIPTION,GRADE_LEVELS,ROLLOVER,bc.TITLE AS CATEGORY
		FROM billing_elements be,billing_categories bc
		WHERE be.SYEAR='" . UserSyear() . "'
		AND be.SCHOOL_ID='" . UserSchool() . "'
		AND be.CATEGORY_ID=bc.ID
		ORDER BY bc.SORT_ORDER IS NULL,bc.SORT_ORDER,CATEGORY,be.REF,be.TITLE" );

	return $elements_RET;
}

/**
 * JS json encoded list of Elements for Select input.
 *
 * @param array $billing_elements Elements array from DB.
 *
 * @return string Json encoded list of Elements.
 */
function BillingElementSelectJSList( $billing_elements )
{
	$elements = [];

	foreach ( (array) $billing_elements as $element )
	{
		$element_title = $element['REF'] ?
			$element['REF'] . ' - ' . $element['TITLE'] :
			$element['TITLE'];

		$elements[ $element['ID'] ] = [
			'AMOUNT' => $element['AMOUNT'],
			'TITLE' => $element_title,
		];
	}

	return json_encode( $elements );
}

/**
 * Elements select options
 *
 * @param array $billing_elements Elements array from DB.
 *
 * @return array Select options.
 */
function BillingElementSelectOptions( $billing_elements )
{
	$element_options = [];

	foreach ( (array) $billing_elements as $element )
	{
		if ( ! isset( $element_options[ $element['CATEGORY'] ] ) )
		{
			$element_options[ $element['CATEGORY'] ] = [];
		}

		$element_title = $element['REF'] ?
			$element['REF'] . ' - ' . $element['TITLE'] :
			$element['TITLE'];

		$element_options[ $element['CATEGORY'] ][ $element['ID'] ] = $element_title;
	}

	return $element_options;
}
